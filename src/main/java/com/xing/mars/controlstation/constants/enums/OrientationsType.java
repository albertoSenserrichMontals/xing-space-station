package com.xing.mars.controlstation.constants.enums;

public enum OrientationsType {
	    /**
	     * First base that was landed in mars
	     */
	 NORTH("N",1,0), 
	 WEST("W",0,-1), 
	 SOUTH("S",-1,0), 
	 EAST("E",0,1) ;
	 
	 private String code;
	 private int xposIncrement ;
	 private int yposIncrement ;
	 
	 private OrientationsType(String code, int yposIncrement,int xposIncrement) {
		 this.code  =code;
		 this.yposIncrement = yposIncrement;
		 this.xposIncrement = xposIncrement;
	 }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}



	public int getXposIncrement() {
		return xposIncrement;
	}

	public void setXposIncrement(int xposIncrement) {
		this.xposIncrement = xposIncrement;
	}

	public int getYposIncrement() {
		return yposIncrement;
	}

	public void setYposIncrement(int yposIncrement) {
		this.yposIncrement = yposIncrement;
	}

	public static OrientationsType getById(String id) {
	    for(OrientationsType e : values()) {
	        if(e.code.equals(id)) return e;
	    }
	    return null;
	 }
	
	 public static String getValidCodes(){
		 StringBuffer buff= new StringBuffer();
		    for(OrientationsType e : values()) {
		    	buff.append(e.getCode());
		    
		    }
		 return buff.toString();
	 }
 }
	 
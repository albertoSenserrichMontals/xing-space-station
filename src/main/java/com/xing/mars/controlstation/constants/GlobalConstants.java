package com.xing.mars.controlstation.constants;

import com.xing.mars.controlstation.constants.enums.ActionsType;
import com.xing.mars.controlstation.constants.enums.OrientationsType;

public class GlobalConstants {

	public static final String DEFAULT_REQUEST_INPUT_FILE_PATH ="D:\\tmp\\testBasePath\\";
	
	public static final String FILE_EXTENSION_FOR_PENDING_REQUEST = "request";
	public static final String FILE_EXTENSION_FOR_PROCESSED_REQUEST = "processed";
	public static final String FILE_EXTENSION_FOR_RESPONSE = "response";
	
	public static final String VALID_NUMBERS_TO_PROCESS = "0987654321";
//	public static final String VALID_CHARACTERSTO_PROCESS = VALID_NUMBERS_TO_PROCESS+OrientationsType.getValidCodes()+ActionsType.getValidCodes();
	
}

package com.xing.mars.controlstation.constants.enums;

public enum ActionsType {
	    /**
	     * First base that was landed in mars
	     */
	 TURN_LEFT('L'), 
	 TURN_RIGHT('R'), 
	 MOVE_FORWARD('M')
	 ;
	 
	 private char code;
	 private int xposIncrement ;
	 private int yposIncrement ;
	 
	 private ActionsType(char code) {
		 this.code  =code;

	 }

	public char getCode() {
		return code;
	}

	public void setCode(char code) {
		this.code = code;
	}



	public int getXposIncrement() {
		return xposIncrement;
	}

	public void setXposIncrement(int xposIncrement) {
		this.xposIncrement = xposIncrement;
	}

	public int getYposIncrement() {
		return yposIncrement;
	}

	public void setYposIncrement(int yposIncrement) {
		this.yposIncrement = yposIncrement;
	}

	public static ActionsType getById(char id) {
	    for(ActionsType e : values()) {
	        if(e.getCode() ==id) return e;
	    }
	    return null;
	 }
	
	 public static String getValidCodes(){
		 StringBuffer buff= new StringBuffer();
		    for(ActionsType e : values()) {
		    	buff.append(e.getCode());
		    
		    }
		 return buff.toString();
	 }
 }
	 
package com.xing.mars.controlstation.constants.enums;

public enum TypeOfCommand {
	    /**
	     * First base that was landed in mars
	     */
		INITIAL_PLATEAU_DEFINITION, VEHICLE_INITIAL_POSITION, ORDER
	}



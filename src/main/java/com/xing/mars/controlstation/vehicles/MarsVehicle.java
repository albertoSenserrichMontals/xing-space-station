package com.xing.mars.controlstation.vehicles;

import com.xing.mars.controlstation.exceptions.ControlStationException;
import com.xing.mars.controlstation.vehicles.components.ActualPosition;
import com.xing.mars.controlstation.vehicles.components.Plateau;

public abstract class MarsVehicle {
	
	protected ActualPosition position;
	protected String ordersToExecute;
	protected boolean thereIsSomeNotification = false;
	protected String dataToReport;
	protected static Plateau areaToExplore;
	
	public boolean isThereIsSomeNotification() {
		return thereIsSomeNotification;
	}

	public void setThereIsSomeNotification(boolean thereIsSomeNotification) {
		this.thereIsSomeNotification = thereIsSomeNotification;
	}

	public String getDataToReport() {
		return dataToReport;
	}

	public void setDataToReport(String dataToReport) {
		this.dataToReport = dataToReport;
	}

	public MarsVehicle(ActualPosition positon, Plateau areaToExplore2) {
		this.position = positon;
		MarsVehicle.areaToExplore = areaToExplore2;
	}
	
	public void setOrdersToExecute(String orderToExecuteI) {
		this.ordersToExecute = orderToExecuteI;
	}
	
	public ActualPosition getPosition() {
		return position;
	}

	public void setPosition(ActualPosition position) {
		this.position = position;
	}

	public String getOrdersToExecute() {
		return ordersToExecute;
	}

	public Plateau getAreaToExplore() {
		return areaToExplore;
	}

	public void setAreaToExplore(Plateau areaToExplore) {
		MarsVehicle.areaToExplore = areaToExplore;
	}

	public abstract void performMovements() throws ControlStationException;
	
	protected abstract void turnRight();
	
	protected abstract void turnLeft();
	
	protected abstract void moveForward() throws ControlStationException;
	
	@Override
	public String toString(){
		return position.getxPos()+" "+position.getyPos()+" " +position.getOrigentation().getCode();
	}
	
	
	
}

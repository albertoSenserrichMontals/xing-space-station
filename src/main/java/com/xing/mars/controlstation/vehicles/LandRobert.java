package com.xing.mars.controlstation.vehicles;

import com.xing.mars.controlstation.constants.enums.ActionsType;
import com.xing.mars.controlstation.constants.enums.ErrorType;
import com.xing.mars.controlstation.constants.enums.OrientationsType;
import com.xing.mars.controlstation.exceptions.ControlStationException;
import com.xing.mars.controlstation.vehicles.components.ActualPosition;
import com.xing.mars.controlstation.vehicles.components.Plateau;


public class LandRobert extends MarsVehicle{
	private int originalXBeforeMov = -1;
	private int originalYBeforeMov = -1;
	
	public LandRobert(ActualPosition positon,Plateau areaToExplore) {
		super(positon,areaToExplore);
	
	}



	@Override
	public void performMovements() throws ControlStationException {
		//1.0 init data
		this.originalXBeforeMov = -1;
		this.originalYBeforeMov = -1;
		super.getAreaToExplore().markFreeSpace(super.getPosition().getxPos(),super.getPosition().getyPos());
		String actionsToExecute = super.getOrdersToExecute();
		if(actionsToExecute!=null && actionsToExecute.length()>0){
			char[] orders = actionsToExecute.toCharArray();
			//2.0 perform actions
			for(char actualOrder: orders){
				ActionsType action = ActionsType.getById(actualOrder);
				switch(action){
			    case TURN_LEFT :
			    	turnLeft();
			    	break; 
			    case TURN_RIGHT :
			    	turnRight();			 
			       break;
			    case MOVE_FORWARD :
			    	moveForward();				     
				       break; 
			    default :
			    	// Invalid action code,  we wont do nothing, 
			    	//TODO: add some error message
			    	break; 
				}	
			}
			super.getAreaToExplore().markOcupiedSpace(super.getPosition().getxPos(),super.getPosition().getyPos());
		}
	}

	@Override
	protected void turnRight() {
		//1.0 init data
		OrientationsType orientation = super.getPosition().getOrigentation();		
		OrientationsType newOrientation = null;
		 //2.0 check transition
		switch(orientation){
	    case NORTH :
	    	newOrientation = OrientationsType.EAST;
	    	break; 
	    case WEST :
	    	newOrientation  = OrientationsType.NORTH;
	 
	       break;
	    case SOUTH :
	    	newOrientation  = OrientationsType.WEST;
		     
		       break; 
	    case EAST :
	    	newOrientation = OrientationsType.SOUTH;     
		       break; 
	    default :
	    	// Invalid action code,  we wont do nothing, 
	    	//TODO: add some error message
	    	break; 	    	
		}
		//3.0 set data if the transition was valid	
		if(newOrientation!=null){
	    	super.getPosition().setOrigentation(newOrientation);
	    }
	}

	@Override
	protected void turnLeft() {
		//1.0 init data
		OrientationsType orientation = super.getPosition().getOrigentation();		
		OrientationsType newOrientation = null;
		 //2.0 check transition
		switch(orientation){
	    case NORTH :
	    	newOrientation = OrientationsType.WEST;
	    	break; 
	    case WEST :
	    	newOrientation  = OrientationsType.SOUTH; 
	       break;
	    case SOUTH :
	    	newOrientation  = OrientationsType.EAST;		     
		       break; 
	    case EAST :
	    	newOrientation = OrientationsType.NORTH;     
		       break; 
	    default :
	    	// Invalid action code,  we wont do nothing, 
	    	//TODO: add some error message
	    	break; 	    	
		}
		//3.0 set data if the transition was valid	
		if(newOrientation!=null){
	    	super.getPosition().setOrigentation(newOrientation);
	    }
	}

	@Override
	protected void moveForward() throws ControlStationException {
		checkNextMovement();
		super.getPosition().addPosX(super.getPosition().getOrigentation().getXposIncrement());
		super.getPosition().addPosY(super.getPosition().getOrigentation().getYposIncrement());
	}

	public void checkNextMovement() throws ControlStationException{
		//1.0 Init base data
		boolean isValid =true;
		int xDestination = super.getPosition().getOrigentation().getXposIncrement()+super.getPosition().getxPos();
		int yDestination = super.getPosition().getOrigentation().getYposIncrement()+super.getPosition().getyPos();
		//2.0 Check if the vehicle is going to exit the plateau (area to explore)
		if(super.getAreaToExplore().getMaxXPos()<xDestination || 0> xDestination){
			
			super.dataToReport = "Abort movement, rovert is going to exit the plateau  keep actual positon ["+super.getPosition().getxPos()+"]["+super.getPosition().getyPos()+"]";
			super.thereIsSomeNotification = true;
			isValid = false;
		}else if(super.getAreaToExplore().getMaxYPos()<yDestination || 0> yDestination){
			super.dataToReport = "Abort movement, rovert is going to exit the plateau keep actual positon ["+super.getPosition().getxPos()+"]["+super.getPosition().getyPos()+"]";		
			super.thereIsSomeNotification = true;
			isValid = false;
		}
		if(!isValid){
			super.getAreaToExplore().markOcupiedSpace(super.getPosition().getxPos(),super.getPosition().getyPos());
			throw new ControlStationException (super.dataToReport, ErrorType.ERROR_OUT_OF_BOUNDARIES_ON_ROVERT_MOVEMENT);
		}
		//3.0 Check collisions before movement
		if(super.getAreaToExplore().isOcupedSpace(xDestination, yDestination)){
			super.dataToReport = "Abort movement. Collision allert to element on coordinates ["+xDestination+"]["+yDestination+"]";		
			super.thereIsSomeNotification = true;
			super.getAreaToExplore().markOcupiedSpace(super.getPosition().getxPos(),super.getPosition().getyPos());
			throw new ControlStationException (super.dataToReport, ErrorType.ERROR_COLLISION_ALERT_AVORTING_MOVEMENT);
		}
	}
 
}

package com.xing.mars.controlstation.vehicles.components;

import com.xing.mars.controlstation.constants.enums.OrientationsType;

public class ActualPosition {

	private int posX;
	private int posY;
	OrientationsType origentation;
	
	public ActualPosition(int xPos, int yPos,OrientationsType origentation) {
		this.origentation = origentation;
		this.posX = xPos;
		this.posY = yPos;
	}
	
	public int getxPos() {
		return posX;
	}
	
	public void setxPos(int xPos) {
		
		this.posX = xPos;
	}
	public int getyPos() {
		return posY;
	}
	
	public void setPosY(int yPos) {
		this.posY = yPos;
	}
	
	public void addPosY(int yPos) {
		this.posY = this.posY+yPos;
	}
	
	public void addPosX(int xPos) {
		this.posX = this.posX+xPos;
	}
	
	public OrientationsType getOrigentation() {
		return origentation;
	}
	
	public void setOrigentation(OrientationsType origentation) {
		this.origentation = origentation;
	}

	
}

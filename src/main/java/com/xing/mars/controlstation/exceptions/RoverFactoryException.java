/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xing.mars.controlstation.exceptions;

import com.xing.mars.controlstation.constants.enums.ErrorType;


public class RoverFactoryException extends ControlStationException{

	public RoverFactoryException(String string, ErrorType err) {
		super(string, err);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2428392086079090457L;
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xing.mars.controlstation.exceptions;

import com.xing.mars.controlstation.constants.enums.ErrorType;


public class RoverMovementException extends ControlStationException{

	public RoverMovementException(String string, ErrorType err) {
		super(string, err);
	
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2960346783222708093L;
    
}

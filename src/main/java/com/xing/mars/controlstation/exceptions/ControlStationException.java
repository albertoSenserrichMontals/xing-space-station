/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xing.mars.controlstation.exceptions;

import org.apache.log4j.Logger;

import com.xing.mars.controlstation.ControlStation;
import com.xing.mars.controlstation.constants.enums.ErrorType;


public class ControlStationException extends Exception{
  /**
	 * 
	 */
	private static final long serialVersionUID = -4586758929813238910L;
	private String msg;
	private final static Logger logger = Logger.getLogger(ControlStation.class);
  
  public ControlStationException(String msg, ErrorType err) {	
		StringBuffer sBuffer = new StringBuffer();
	    sBuffer.append(err.getCode());
	    sBuffer.append(msg);
		logger.error(sBuffer.toString());
  }

	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
  
  
}

package com.xing.mars.controlstation;

import org.apache.log4j.Logger;

import com.xing.mars.controlstation.constants.enums.BaseType;
import com.xing.mars.controlstation.factory.ControlStationFactory;

import controlStation.executors.ControlStationExecutor;


public class ControlStation {
	
	private final static Logger logger = Logger.getLogger(ControlStation.class);

	public static void main(String[] args) {
       logger.debug("Space Station is on-line");
       //1.0 Instanciate type of base   
       ControlStationExecutor mrbase = ControlStationFactory.getBase(BaseType.NASA_MARS_CONTROL_STATION);      
       //2.0 Perform base action of base (process orders)
       mrbase.performActions();      
       logger.debug("Space Station is shuting down, end execution of request");
       
	}

}

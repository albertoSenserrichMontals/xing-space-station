package com.xing.mars.controlstation.requestControlers;

import java.util.List;

import com.xing.mars.controlstation.exceptions.ControlStationException;
import com.xing.mars.controlstation.exceptions.ControlStationInputDataException;



public interface RequestControler {

	public void markRequestProcessed(String requestId)  throws ControlStationException;
	public void  returnAnswer(String requestId, List<String> responseData) throws ControlStationInputDataException;
	public List<String> readRequest(String requestId) throws ControlStationException;	
	public List<String> returnListOfRequestToProcess() ;
	
}

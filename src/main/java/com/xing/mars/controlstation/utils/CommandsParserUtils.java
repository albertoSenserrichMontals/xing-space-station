package com.xing.mars.controlstation.utils;

import com.xing.mars.controlstation.constants.enums.OrientationsType;
import com.xing.mars.controlstation.constants.enums.TypeOfCommand;
import com.xing.mars.controlstation.vehicles.components.ActualPosition;
import com.xing.mars.controlstation.vehicles.components.Plateau;


public final class   CommandsParserUtils {


	
	public static Plateau parsePlateau (String line){
        Plateau plateau;
        int pos = line.lastIndexOf(" ");
        int maxX, maxY;
        try {
            maxX = Integer.parseInt(line.substring(0,pos).trim());
            maxY = Integer.parseInt(line.substring(pos+1,line.length()).trim());
        } catch (NumberFormatException e) {
            maxX = -1;
            maxY = -1;
        }
        plateau = new Plateau(maxX, maxY);
        
		return plateau;
	}
	
	public static ActualPosition parseActualPosition (String line){
		ActualPosition actualPoss;
        line = line.trim();
        int pos = line.indexOf(" ");
        int last = line.lastIndexOf(" ");
        int maxX, maxY;
        OrientationsType origentation;
    	
        try {
            maxX = Integer.parseInt(line.substring(0,pos));
            maxY = Integer.parseInt(line.substring(pos+1,last));
        } catch (NumberFormatException e) {
            maxX = 10;
            maxY = 10;
        }
        origentation = OrientationsType.getById(line.substring(last+1,line.length()));
        actualPoss = new ActualPosition(maxX, maxY,origentation);
        
		return actualPoss;
	}
	
	
	public static  TypeOfCommand checkTypeOfCommand(String line){
		line = line.trim();
		if(line.split(" ").length==3){
			return TypeOfCommand.VEHICLE_INITIAL_POSITION;
		}else{
			return TypeOfCommand.ORDER;
		}
	}
	
	private static  boolean isValidCommand(String line){
		boolean rsl = false;
		line= line.replaceAll(" ", "");
		for(int i=0;i<line.length();i++){
//			VALID_CHARACTERSTO_PROCESS
//			GlobalCo
		}
		return rsl;
	}
}

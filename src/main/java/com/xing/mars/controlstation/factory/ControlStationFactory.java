package com.xing.mars.controlstation.factory;

import com.xing.mars.controlstation.constants.GlobalConstants;
import com.xing.mars.controlstation.constants.enums.BaseType;

import controlStation.executors.ControlStationExecutor;
import controlStation.executors.impl.MarsControlStationExecutor;

public class ControlStationFactory {

	
    /**
     * Get a base Instance by type
     * @param input Input string with the coordinates of the right north border
     * of area to explore, and all orders to do by the rovers
     * @param base Type of base to be created
     * @return MarsBase
     */
    public static ControlStationExecutor getBase( BaseType base) {
    	//1.0 init basic values
    	String path = GlobalConstants.DEFAULT_REQUEST_INPUT_FILE_PATH;
    	ControlStationExecutor defaultBaseExecutor  = null;
        switch (base) {
            case NASA_MARS_CONTROL_STATION:
            	defaultBaseExecutor = new MarsControlStationExecutor(path);
                break;
            default:
            	
                break;
        }
        return defaultBaseExecutor;
    }
}

package com.xing.mars.controlstation;

import com.xing.mars.controlstation.constants.enums.OrientationsType;

public class Coordinates {

    private int xpos;
    private int ypos;
    OrientationsType origentation;
    
    public Coordinates() {
    }
    
    public Coordinates(int xpos, int ypos, OrientationsType origentation) {
        this.xpos = xpos;
        this.ypos = ypos;
        this.origentation = origentation;
    }

	public OrientationsType getOrigentation() {
		return origentation;
	}

	public void setOrigentation(OrientationsType origentation) {
		this.origentation = origentation;
	}

	public int getXpos() {
		return xpos;
	}

	public void setXpos(int xpos) {
		this.xpos = xpos;
	}

	public int getYpos() {
		return ypos;
	}

	public void setYpos(int ypos) {
		this.ypos = ypos;
	}
    
    
}

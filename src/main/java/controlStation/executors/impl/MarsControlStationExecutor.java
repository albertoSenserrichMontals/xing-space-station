package controlStation.executors.impl;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.xing.mars.controlstation.ControlStation;
import com.xing.mars.controlstation.constants.enums.TypeOfCommand;
import com.xing.mars.controlstation.exceptions.ControlStationException;
import com.xing.mars.controlstation.exceptions.ControlStationInputDataException;
import com.xing.mars.controlstation.requestControlers.RequestControler;
import com.xing.mars.controlstation.requestControlers.impl.FileRequestControler;
import com.xing.mars.controlstation.utils.CommandsParserUtils;
import com.xing.mars.controlstation.vehicles.LandRobert;
import com.xing.mars.controlstation.vehicles.MarsVehicle;
import com.xing.mars.controlstation.vehicles.components.ActualPosition;
import com.xing.mars.controlstation.vehicles.components.Plateau;

import controlStation.executors.ControlStationExecutor;

public class MarsControlStationExecutor implements ControlStationExecutor{

	private  String basePath = null;
	RequestControler reqConTroler =null;
	private final static Logger logger = Logger.getLogger(MarsControlStationExecutor.class);
	
	public MarsControlStationExecutor(String basePath){
		this.basePath = basePath;
		this.reqConTroler = new  FileRequestControler(basePath);
	}
	
	public void performActions() {
		// TODO Auto-generated method stub
	
		//1.0 REtrieve list of request of process
		List<String>  reqToProcess = reqConTroler.returnListOfRequestToProcess();		
		for(String requestId : reqToProcess){			
			//2.0 Parse input information
			List<String>  orders = null;
			try{
				  orders = reqConTroler.readRequest(requestId);
			}catch(ControlStationException e){
				
			}			
			MarsVehicle tmpVehicle = null;
			List<MarsVehicle> vehicles = new  LinkedList<MarsVehicle>();
			if(orders!=null){
				Plateau plateau = 	CommandsParserUtils.parsePlateau(orders.get(0));
				for(int i=1;i+1<orders.size();i=i+2){
					ActualPosition actualPoss = CommandsParserUtils.parseActualPosition(orders.get(i));
					tmpVehicle = new LandRobert(actualPoss,plateau);
					tmpVehicle.setOrdersToExecute(orders.get(i+1).trim());	
					vehicles.add(tmpVehicle);
					plateau.markOcupiedSpace(tmpVehicle.getPosition().getxPos(), tmpVehicle.getPosition().getyPos());
				}
			}			
			//3.0 Check if the movement can be performed, and it that case execute the movement
			int i=0;
			for(MarsVehicle vechicle: vehicles){
				try{
					vechicle.performMovements();
				}catch(ControlStationException e ){
					logger.error("Error performing movement on request ["+requestId+"] for vehicle number ["+i+"] with message ["+e.getMessage()+"]");
				}
				i++;				
			}			
			//4.0 Check if the movement can be performed, and it that case execute the movement
			List<String> dataToReturn  =  new  LinkedList<String>();
			for(MarsVehicle vehicle: vehicles){
				if(!vehicle.isThereIsSomeNotification()){
					dataToReturn.add(vehicle.toString());
				}else{
					dataToReturn.add(vehicle.toString());
					//Dissabled feature #004 we need aprovation before we show the error codes. Another teams should integrate this changes on their parsers in order to use our response					
//					dataToReturn.add(vehicle.toString() +" -  " +vehicle.getDataToReport());
				}
			}
			//5.0 Save response into system
			boolean error = false;
			try{
				if(dataToReturn!=null &&dataToReturn.size()>0){
					reqConTroler.returnAnswer(requestId, dataToReturn);
				}
			}catch (ControlStationInputDataException e){
				 error = true;
				 logger.error("There was an error saving data from  requestId ["+requestId+"], exception detaill ["+e.getMessage()+"]");
			}
			//6.0 Mark request as succefull executed
			try{			
				if(!error){
					reqConTroler.markRequestProcessed(requestId);
				}
			}catch(ControlStationException e){
				logger.error("There was an error marking requestId ["+requestId+"] as resolved, exception detaill ["+e.getMessage()+"]");
			}
			
		}
		
	
		
	
	}

}

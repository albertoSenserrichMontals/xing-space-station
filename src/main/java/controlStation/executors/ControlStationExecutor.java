package controlStation.executors;

public interface ControlStationExecutor {

	public void performActions();
}

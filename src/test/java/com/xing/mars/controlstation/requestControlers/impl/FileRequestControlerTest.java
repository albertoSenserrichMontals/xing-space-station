package com.xing.mars.controlstation.requestControlers.impl;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.xing.mars.controlstation.constants.GlobalConstants;
import com.xing.mars.controlstation.constants.enums.ErrorType;
import com.xing.mars.controlstation.exceptions.ControlStationException;
import com.xing.mars.controlstation.exceptions.ControlStationInputDataException;
import com.xing.mars.controlstation.requestControlers.RequestControler;




public class FileRequestControlerTest {
	private final static String TEMP_TEST_BASE_PATH ="D:\\tmp\\testBasePath\\"; //Path for test preparation
	//FIXME: Updata path in order to get a relativa path
	private final static String TEMP_RESOURCE_TEST  =  "D:\\workspace\\controlstation\\src\\test\\resources\\inputRequestFolder\\";	
//	private final static String TEMP_RESOURCE_TEST  = ClassLoader.getSystemResource("inputRequestFolder/inputRequestResponsesFolder/").getPath(); //Path to original files for testing
	RequestControler reqControler = new FileRequestControler(TEMP_TEST_BASE_PATH);
	
	private final static String VALID_PROCESS_ID_1 ="inputRequest1";
	private final static String VALID_PROCESS_ID_2 ="inputRequest2";
	private final static String VALID_PROCESS_ID_3 ="inputRequest3";
	private final static String INVALID_PROCESS_ID_1 ="inputRequest3Invalid";
	private final static String INVALID_PROCESS_EXTENSIOn_ID_1 ="inputRequest3.requestInvalidExtension";
	
	
	private String VALID_PROCESS_ID_1_LINE_2 ="1 2 N";
	private String VALID_PROCESS_ID_1_LINE_1 ="5 5";
	private String VALID_PROCESS_ID_1_RESPONSE ="1 3 N";
	
	@After
	public void cleanUpAfterTest(){
		File folder = new  File(TEMP_TEST_BASE_PATH); 
		try {
			FileUtils.cleanDirectory(folder);
		} catch (IOException e) {
			fail("Error during clean up["+e.getMessage()+"]");
		} 
	}
	
	@Before
	public void beforeAnyTest(){
		File folder = new  File(TEMP_TEST_BASE_PATH); 
		if(!folder.exists()){
			folder.mkdir();
		}
	}
		
	@Test
	public void markRequestProcessedValid() throws ControlStationException{
		copyResourceToTempFolder(VALID_PROCESS_ID_1+"."+GlobalConstants.FILE_EXTENSION_FOR_PENDING_REQUEST);
		reqControler.markRequestProcessed(VALID_PROCESS_ID_1);
		String path = TEMP_TEST_BASE_PATH + VALID_PROCESS_ID_1+"."+ GlobalConstants.FILE_EXTENSION_FOR_PROCESSED_REQUEST;
		File processedFile = new File(path);
		assertTrue("Valid files cannot be processd",processedFile.exists());
	}
	
	@Test(expected=ControlStationInputDataException.class)
	public void markRequestProcessedInValid() throws ControlStationException{
		copyResourceToTempFolder(INVALID_PROCESS_EXTENSIOn_ID_1);
		reqControler.markRequestProcessed(INVALID_PROCESS_ID_1);
		String path = TEMP_TEST_BASE_PATH + INVALID_PROCESS_ID_1+"."+ GlobalConstants.FILE_EXTENSION_FOR_PROCESSED_REQUEST;
		File processedFile = new File(path);
		fail("Invalid files cannot be processd");
	}

	@Test(expected=ControlStationInputDataException.class)
	public void  readINValidRequest() throws ControlStationException{
		copyResourceToTempFolder(INVALID_PROCESS_EXTENSIOn_ID_1);
		reqControler.readRequest(INVALID_PROCESS_EXTENSIOn_ID_1);
	}
	
	@Test
	public void readValidRequest() throws ControlStationException{
		copyResourceToTempFolder(VALID_PROCESS_ID_1+"."+GlobalConstants.FILE_EXTENSION_FOR_PENDING_REQUEST);
		List<String> result = reqControler.readRequest(VALID_PROCESS_ID_1);
		assertEquals(VALID_PROCESS_ID_1_LINE_1 ,result.get(0).trim());
		assertEquals(VALID_PROCESS_ID_1_LINE_2 , result.get(1).trim());		
	}
	
	
	@Test
	public void noFilesToProcess() {
		List<String> filesToProcess = reqControler.returnListOfRequestToProcess();
		assertTrue(filesToProcess.size() == 0);
	}
	
	@Test
	public void onlyProceesedFiles()  {
		copyResourceToTempFolder(INVALID_PROCESS_EXTENSIOn_ID_1);
		List<String> filesToProcess = reqControler.returnListOfRequestToProcess();
		assertTrue(filesToProcess.size() == 0);
	}
	
	@Test
	public void onlyOneFileToProcess() {
		copyResourceToTempFolder(VALID_PROCESS_ID_1+"."+GlobalConstants.FILE_EXTENSION_FOR_PENDING_REQUEST);
		List<String> filesToProcess = reqControler.returnListOfRequestToProcess();
		assertTrue(filesToProcess.size() == 1);
	}
	
	@Test
	public void multipleFilesToProcess()  {
		copyResourceToTempFolder(VALID_PROCESS_ID_1+"."+GlobalConstants.FILE_EXTENSION_FOR_PENDING_REQUEST);
		copyResourceToTempFolder(VALID_PROCESS_ID_2+"."+GlobalConstants.FILE_EXTENSION_FOR_PENDING_REQUEST);
		copyResourceToTempFolder(VALID_PROCESS_ID_3+"."+GlobalConstants.FILE_EXTENSION_FOR_PENDING_REQUEST);
		List<String> filesToProcess = reqControler.returnListOfRequestToProcess();
		assertTrue(filesToProcess.size() == 3);
	}
	
	
	public List<String> returnListOfRequestToProcess(String basePath){
		return null;
	}
	
	private void copyResourceToTempFolder(String id){
		String path =TEMP_RESOURCE_TEST+id;
		 try {
			FileUtils.copyFileToDirectory(new File(path), new File(TEMP_TEST_BASE_PATH));
		} catch (IOException e) {
			fail("Error during initialization up["+e.getMessage()+"]");
		}
	}
	
	
	public List<String> readRequest(String path) throws ControlStationInputDataException{
		//1.0 Init data
		List<String> response = new LinkedList<String>();
		
		//2.0 Read fuke
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	response.add(line);
		    }
		} catch (IOException e) {
			fail("Error during initialization up["+e.getMessage()+"]");
		}
		return response;	
		
	}
	
}
